'use strict';
let express = require('express');
let router = express.Router();
let Promise = require('bluebird');
//let Models = db.sequelize.models;

let redis = require('redis');
let client = redis.createClient();
let book = require('../models/book');
let giftRoute = require('./gift/index');
let controllers = require('../controllers/book');

//const excelToJson = require('convert-excel-to-json');
//let cron = require('../libs/CheckCronJob');

/* GET home page. */
router.use('/gift',giftRoute);

router.get('(/page/:page)?', controllers.getAllBook);

router.get('/add_book', function (req, res) {
    res.render('file', {
        title: 'Thêm sách lên hệ thống',
        urlForm: '/add_book',
        method: 'POST',
        submit_btn_label: 'Thêm Liên Kết',
        nav: 'add_book'
    });
});
router.get('/add_gift', function (req, res) {
    res.render('file', {
        title: 'Thêm quà tặng lên hệ thống',
        urlForm : '/add_gift',
        method: 'POST',
        submit_btn_label: 'Thêm Liên Kết',
        nav: 'add_gift'
    });
});
router.post('/add_book', controllers.addBook);
router.post('/add_gift', controllers.addGift);

router.get('/add', function (req, res) {
    res.render('add', {
        title: 'Thêm liên kết theo dõi',
        menu: 'add',
        method: 'POST',
        submit_btn_label: 'Thêm Liên Kết'
    });
});
router.post('/add', function (req, res) {
    let form = req.body;
    if (form.name === '' || form.name == null) {
        res.render('add', {
            title: 'Thêm liên kết theo dõi',
            menu: 'add',
            error: {
                name: 'Vui lòng nhập tên'
            }
        });
    } else if (form.url === '' || form.url == null) {
        res.render('add', {
            title: 'Thêm liên kết theo dõi',
            menu: 'add',
            error: {
                url: 'Vui lòng nhập liên kết'
            }
        });
    } else {

        let user = req.session.user;
        //console.log(JSON.stringify(user,2,2));
        Promise.resolve(isOutOfStock.isOut(form.url))
            .then(function (isOutOfStock) {

                form.status = isOutOfStock;
                form.user_id = user.id;
                return Models.link.create(form, {
                    raw: true
                })
            })
            .then(_link => {
                res.redirect('/');
            }).catch(err => {
            res.render('add', {
                title: 'Thêm liên kết theo dõi',
                menu: 'add',
                error: 'Lỗi khi tạo liên kết !\\n' + err.message
            });
        });


    }

});
router.get('/delete', function (req, res) {
    let link_id = req.query.id;
    //console.log(link_id);
    Models.link.findById(link_id)
        .then(_link => {
            //req.flash('mes','Xóa Thành Công')
            return _link.destroy();
        })
        .then(function () {
            res.redirect('/');
        })
        .catch(err => {
            //r//eq.flash('err','Xóa Thành Công')
            res.redirect('/');
        })

});
router.get('/edit', controllers.getEditBook);
router.post('/edit', function (req, res) {
    let form = req.body;
    //console.log(form.id);
    Models.link.findById(form.id)
        .then(_link => {
            return _link.update(form)
        })
        .then(_link => {
            //console.log(_link);
            res.render('edit', {
                title: 'Chỉnh sửa liên kết',
                menu: 'add',
                link: _link
            });
        })
        .catch(err => {
            res.render('edit', {
                title: 'Chỉnh sửa liên kết',
                menu: 'add',
                error: err.message
            });
        })

});

router.get('/config', function (req, res) {
    client.get('check_tiki_config_email', (err,data)=>{
        if (err){
            console.log('check_tiki_config_email is null');
            res.render('config', {
                title: 'Cấu hình thông báo',
                menu: 'config',
                config: null
            });
        }else{
            //console.log(typeof data);
            res.render('config', {
                title: 'Cấu hình thông báo',
                menu: 'config',
                config: JSON.parse(data)
            });
        }
    });
});
router.post('/config', function (req, res) {
    let form = req.body;
    client.set('check_tiki_config_email',JSON.stringify(form), function (err) {
        if (err){
            console.log(err);
            res.render('config', {
                title: 'Cấu hình thông báo',
                menu: 'config',
                config: form,
                error: 'Cấu hình không thành công !'
            });
        }else{
            //console.log(data);
            cron(client)
                .then(function (_result) {
                    //console.log(_result);
                    res.render('config', {
                        title: 'Cấu hình thông báo',
                        menu: 'config',
                        config: form,
                        msg: 'Cấu hình thành công !'
                    });
                }).catch(function (err) {
                console.log(err);
            });
        }
    });

});
module.exports = router;

