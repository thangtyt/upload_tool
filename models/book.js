var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BookSchema = new Schema({
    bookId: String,
    code: String,
    title: String,
    shortDescription: String,
    fullDescription: String,
    instruction: String,
    fullInstruction: String,
    cover: String,
    icon: String,
    author: [{
        authorId: String,
        name: String,
        avatar: String
    }],
    parts: [{
        title: String,
        partId : String,
        audios: [{
            audioId: String,
            title: String,
            duration: Number,
            totalRepeat: Number,
            page: Number
        }]
    }],
    commitment: [],
    createdDate: {
		type: Date,
		default: Date.now
	},
    progressRule: String //soft-lock-all, unlock-all, lock-all
});

module.exports = mongoose.model('Book', BookSchema);