var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AudioSchema = new Schema({
    audioId: String,
    title: String,
    duration: Number,
    source: String,
    totalRepeat: Number,
    cc: [{
        time: Number,
        text: String
    }]
});

module.exports = mongoose.model('Audio', AudioSchema);