let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let GiftCodeSchema = new Schema({
    gifts: [{
        gift: {
            giftId: String,
            title: String,
            source: String,
            type: String,
            fullDescription: String
        }
    }],
    giftCode: {
        type : String,
        validate: {
            validator: function(v, cb) {
                //console.log('222222');
                GiftCodeSchema.find({giftCode: v}, function(err,docs){
                    cb(docs.length == 0);
                });
            },
            message: 'GiftCode đã có trên hệ thống vui lòng nhập mã khác'
        }
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    cover: String
});

module.exports = mongoose.model('Giftcode', GiftCodeSchema);