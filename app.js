'use strict';
let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let nunjucks = require('nunjucks');
let session = require('express-session');
let Redis = require('connect-redis')(session);
let cors = require('cors');
let index = require('./routes/index');
//let users = require('./routes/users');
let login = require('./routes/login');
let register = require('./routes/register');
let app = express();
let mongoose = require('mongoose');
let config = require('./configs/default');

//add mongodb to connect
mongoose.connect("mongodb://"+config.database.mongodb.host+"/"+config.database.mongodb.dbname,{
    // sets how many times to try reconnecting
    reconnectTries: 5000,
    // sets the delay between every retry (milliseconds)
    reconnectInterval: 100
});
mongoose.set('debug', true);
let db = mongoose.connection;
db.on('error', console.error.bind(console,'connection error: '));
db.once('open',function () {
   console.log('Ket noi thanh cong');
});
//add redis to save session
// app.use(session({
//     store: new Redis({
//         host: 'localhost',
//         port: 6379,
//         prefix: 'UploadToolsSess:'
//     }),
//     secret: 'qwertyuiop',
//     resave: false,
//     saveUninitialized : false
// }));




// view engine setup
// app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
let env = nunjucks.configure(path.join(__dirname, 'views'),{
    autoescape: true,
    express: app
});
//add custom filter nunjucks
require('./libs/pagination')(env);
require('./libs/datetime')(env);


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/register',register);
app.use('/login', login);
//check session
// app.use(function (req,res,next) {
//     if (req.session.user){
//         res.locals.user = req.session.user;
//         next();
//     }else{
//         res.redirect('/login');
//     }
// });

app.use('/', index);
// app.use('/user', users);

app.use('/logout', function(req, res) {
    req.session.user = null;
    res.render('login', { title: 'Login Page' });
});
app.use(function (req,res) {
    res.render('error',{
        message: 'Page not found !'
    });
});


module.exports = app;
