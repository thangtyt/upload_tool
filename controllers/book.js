let BOOK = require('../models/book');
let AUDIO = require('../models/audio');
let GIFTCODE = require('../models/giftcode');
let formidable = require('formidable');
const excelToJson = require('convert-excel-to-json');
let fs = require('fs');
let slug = require('slug');
let uuid = require('uuid/v4');

module.exports = {
    addGift : function (req,res) {
        let form = formidable.IncomingForm();
        let gifts = [];
        let giftJSON = [];
        let giftCodes = [];
        let serverUrl = "https://media.mcbooksapp.com/mcbooks-gifts/";
        form.parse(req, function (err, fields, files) {
            const giftExcel = excelToJson({
                sourceFile: files.file.path
            });
            //console.log(JSON.stringify(giftExcel['gifts'],2,2));
            let title = "";
            let description = "";
            //console.log(JSON.stringify(giftExcel,2,2));
            if(giftExcel.hasOwnProperty('info')){
                title = giftExcel['info'][0]['B'];
                serverUrl += slug(title.toLowerCase());
                description = giftExcel['info'][1]['B'];
            }
            if(giftExcel.hasOwnProperty('gifts')){
                console.log(giftExcel['gifts']);
                giftExcel['gifts'].map(function (val) {
                    let fileType = '';
                    console.log(val['C']);
                    if(val['C'].toLowerCase().trim() == 'book' || val['C'].toLowerCase().trim() == 'ebook'){
                        fileType = '.pdf'
                    }else if(val['C'].toLowerCase().trim() ==  'audio'){
                        fileType = '.mp3'
                    }else{
                        fileType = '.mp4'
                    }
                    giftJSON.push({
                        giftId:uuid(),
                        title : val['A'],
                        source : serverUrl+"/"+val['B']+fileType,
                        fullDescription : description,
                        type : val['C']
                    })
                })
            }
            if(giftExcel.hasOwnProperty('code')){
                //console.log(giftExcel['code']);
                giftExcel['code'].map(function (val) {
                    //console.log(typeof val['A']);
                    let temp = val['A'] + '';
                    gifts.push(temp.trim());
                    giftCodes.push({
                        giftCode : temp.trim(),
                        cover : serverUrl+"/giftcover.png",
                        gifts : giftJSON
                    });
                });
                fs.unlink(files.file.path, function () {
                });//xoa file rac
                //console.log(JSON.stringify(giftCodes,2,2));
                GIFTCODE.find({
                    giftCode : {
                        $in : gifts
                    }
                }).then(function (result) {
                    //console.log(JSON.stringify(result,2,2));
                    if(result.length > 0){
                        throw new Error('Có mã đã có trong hệ thống.');
                    }else{
                        return GIFTCODE.collection.insert(giftCodes);
                    }
                }).then(function (result) {
                    //console.log(JSON.stringify(result,2,2));
                    res.sendStatus(200);
                }).catch(function (err) {
                    console.log("----- LOI UP GIFTCODE : -----",err);
                    res.sendStatus(300);
                })
            }else{
                res.sendStatus(300);
            }

        });
    },
    addBook : function (req,res) {
        let form = formidable.IncomingForm();
        let bookJSON = {
            bookId : uuid(),
            commitment : [],
            author : [],
            parts : []
        };
        let audioJSON = [];
        let serverUrl = "https://media.mcbooksapp.com/mcbooks-media/";
        form.parse(req, function (err, fields, files) {
            //console.log(JSON.stringify(files.file.path));
            const bookExcel = excelToJson({
                sourceFile: files.file.path
            });

            if(bookExcel.hasOwnProperty('info')){
                bookJSON.code = bookExcel['info'][0]["B"];
                bookJSON.title = bookExcel['info'][1]["B"];
                serverUrl = serverUrl + slug(bookJSON.title.toLowerCase());
                bookJSON.shortDescription = bookExcel['info'][2]["B"];
                bookJSON.fullDescription = serverUrl+"/description.html";
                bookJSON.instruction = bookExcel['info'][3]["B"];
                bookJSON.fullInstruction = serverUrl+"/instruction.html";
                bookJSON.cover = serverUrl+"/img/cover.png";
                bookJSON.icon = serverUrl+"/img/icon.png";
                bookJSON.progressRule = "unlock-all";
            }
            if(bookExcel.hasOwnProperty('author')){
                //console.log(bookExcel['author'].length);
                let tempAuthor = 0;
                bookExcel['author'].map(function (ele) {

                    if (ele['A'].toLowerCase() !== 'name' ){

                        bookJSON['author'].push({
                            authorId : uuid(),
                            name : ele['A'],
                            avatar : serverUrl+"/img/author"+(tempAuthor == 0 ? "" : tempAuthor)+".png"
                        });
                        tempAuthor++;
                    }
                })
            }
            if(bookExcel.hasOwnProperty('parts')){
                let fileName = 1;
                bookExcel['parts'].map(function (_ele,index) {
                    if (index > 0){
                        if (_ele.hasOwnProperty('A')){
                            bookJSON['parts'].push({
                                title : _ele['A'],
                                partId : uuid(),
                                audios : []
                            });
                        }else{
                            let audioID = uuid();
                            let fileType = '';

                            //let audioName = bookJSON['parts'][bookJSON['parts'].length-1].title + " "+_ele['B'];
                            //let audioName = Number(_ele['B'].split(' ').pop());
                            let audioName = fileName < 10 ? '0'+fileName : fileName;
                            fileName++;
                            //console.log(audioName);
                            bookJSON['parts'][bookJSON['parts'].length-1]['audios'].push({
                                audioId : audioID,
                                title : _ele['B'],
                                duration : Number(_ele['E']),
                                page : Number(_ele['C']),
                                totalRepeat : Number(_ele['D'])
                            });
                            if(_ele['F'].toLowerCase().trim() == 'audio'){
                                fileType = '.mp3'
                            }else if(_ele['F'].toLowerCase().trim() == 'ebook' || _ele['F'].toLowerCase().trim() == 'book'){
                                fileType = '.pdf'
                            }else{
                                fileType = '.mp4'
                            }
                            audioJSON.push({
                                audioId: audioID,
                                title : _ele['B'],
                                duration : Number(_ele['E']),
                                page : Number(_ele['C']),
                                totalRepeat : Number(_ele['D']),
                                //source : serverUrl+"/"+slug(audioName.toLowerCase())+fileType,
                                source : serverUrl+"/"+audioName+fileType,
                                cc : []

                            })
                        }
                    }
                });
            }
            // console.log(JSON.stringify(audioJSON,2,2));
            fs.unlink(files.file.path, function () {
            });//xoa file rac

            //console.log(JSON.stringify(bookJSON,2,2));
            BOOK
                .find({
                    code : bookJSON.code
                })
                .then(function (result) {
                    //console.log(JSON.stringify('----',bookJSON,2,2));
                    if(result.length > 0){
                        throw Error('Đã Tồn tại cuốn sách này .');
                    }else{
                        return 'ok'
                    }
                })
                .then(function (_result) {
                    return BOOK.create(bookJSON);
                })
                .then(function (_result) {
                    //console.log(JSON.stringify(audioJSON,2,2));
                    return AUDIO.insertMany(audioJSON);
                })
                .then(function (_result) {
                    //console.log(_result);
                    res.sendStatus(200);
                })
                .catch(function (err) {
                    console.error(err.message);
                    res.sendStatus(300);
                });
        });
    },
    getAllBook : function (req,res) {
        let itemOfPage = 20;
        let page = req.params.page || 1;
        let param = req.query;
        let order = [];
        let totalRecord = 0;
        BOOK
            .count()
            .then(function (_totalRecord) {
                totalRecord = _totalRecord;
                //console.log(_totalRecord);
                BOOK
                    .find({})
                    .select(['_id','title','code','createdDate'])
                    .limit(itemOfPage)
                    .skip((page-1)*itemOfPage)
                    .sort({ createdDate: -1 })
                    .then(function (books) {
                        //console.log(JSON.stringify(books,2,2));
                        let totalPage = Math.ceil(totalRecord / itemOfPage);
                        res.render('index',
                            {
                                title: 'Danh sách liên kết',
                                menu: 'list',
                                books: books,
                                totalCount: totalRecord,
                                totalPage: totalPage,
                                itemOfPage: itemOfPage,
                                //order: param.status || 'ASC',
                                page: page,
                                nav: 'list_books'
                            });
                    })
            })
    },
    getEditBook : function (req,res) {
        let id =  req.query.id;
        BOOK.findById(id,function (err,_book) {
            //console.log(typeof _book.code);
            console.log(JSON.parse(JSON.stringify(_book,2,2)));
            res.render('add', {
                title: 'Thay đổi thông tin sách',
                menu: 'add',
                author: JSON.parse(JSON.stringify(_book,2,2)),
                method: 'put',
                submit_btn_label: 'Thay đổi'
            });
        });
    }
};