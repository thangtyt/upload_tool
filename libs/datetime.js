'use strict';
let moment = require('moment');
module.exports = function (env) {
    env.addFilter('date',function (input,format) {
        // console.log('filter : '+ moment(input).format(format));
        if (input !== '' && input !=null){
            return moment(input).format(format);
        }else{
            return '';
        }
    });
    return env;
};