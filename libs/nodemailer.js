/**
 * Created by thangtyt on 12/19/17.
 */
const nodemailer = require('nodemailer');

module.exports = function(account,mailOptions){
    "use strict";
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: account.user, // generated ethereal user
            pass: account.pass  // generated ethereal password
        }
    });

    // send mail with defined transport object
    return new Promise((resolve, reject)=>{
        transporter.sendMail(mailOptions, (error, info) => {
            if (error)
                reject(error);
            //console.log('Message sent: ',info);
            transporter.close();
            resolve(info)
        });
    })
};